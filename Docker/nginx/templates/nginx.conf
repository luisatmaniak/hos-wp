user nginx;
worker_processes auto;
pid /var/run/nginx.pid;

events {
        worker_connections 1024;
        multi_accept on;
        use epoll;
}

http {
        ##################
        #Filehandle Cache#
        ##################

        open_file_cache          max=2000 inactive=20s;
        open_file_cache_valid    60s;
        open_file_cache_min_uses 5;
        open_file_cache_errors   off;

        ##################
        # Basic Settings #
        ##################

        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
        keepalive_timeout 5;
        #types_hash_max_size 2048;
        server_tokens off;

        # server_names_hash_bucket_size 64;
        # server_name_in_redirect off;

        include /etc/nginx/mime.types;
        default_type application/octet-stream;

        ####################
        # Logging Settings #
        ###################

        log_format x-log '$http_x_forwarded_for - [$time_local] ' '"$request" $status $body_bytes_sent "$http_referer" ' '"$http_user_agent"';

        access_log /var/log/nginx/access.log x-log;
        error_log /var/log/nginx/error.log;

        #################
        # Gzip Settings #
        #################

        gzip on;
        gzip_min_length 1100;
        gzip_vary on;
        gzip_proxied any;
        gzip_comp_level 8;
        gzip_buffers 32 8k;
        gzip_http_version 1.1;
        gzip_types application/atom+xml application/javascript application/json application/ld+json application/manifest+json
                   application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/x-font-ttf application/x-web-app-manifest+json
                   application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard
                   text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;
        gzip_disable "MSIE [4-6].(?!.*SV1)";


        ###############
        # Buffer Size #
        ###############

        client_body_buffer_size      128k;
        client_max_body_size         60M;
        client_header_buffer_size    1k;
        large_client_header_buffers  4 4k;
        output_buffers               1 32k;
        postpone_output              1460;

        ##
        # nginx-naxsi config
        ##
        # Uncomment it if you installed nginx-naxsi
        ##

        #include /etc/nginx/naxsi_core.rules;

        ##
        # nginx-passenger config
        ##
        # Uncomment it if you installed nginx-passenger
        ##

        #passenger_root /usr;
        #passenger_ruby /usr/bin/ruby;

        ##
        # Virtual Host Configs
        ##

        include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/sites-enabled/*;

}
